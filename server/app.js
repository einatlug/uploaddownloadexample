const express = require('express');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const bodyParser = require('body-parser');
const cors = require('cors');
const uploadsDir = "./uploads";

var app = express();
app.use(bodyParser.json());

// Configure the storage space for the uploaded files. 
var uploadsStorage = multer.diskStorage({
    // You can set the destination and send a custome error here.
    destination: function(req, file, callback){
        // If a username directory does not exist, create it. 
        if(!fs.existsSync(uploadsDir + "/" + req.body.username)){
            fs.mkdirSync(uploadsDir + "/" + req.body.username);
        }

        // Set the destination to the newly created folder.
        var error = null;
        callback(error, uploadsDir + "/" + req.body.username);
    },
    // You can also set a custome filename here. This appends the date and time to the filename.
    filename: function(req, file, callback){
        var error = null;
    
        // Check if there is already a file under this name.
        if(fs.existsSync(uploadsDir + "/" + req.body.username + "/" + req.body.uploadedFileName)){
            error = new Error("There is already a file under that name. Choose a different name.");
        }
    
        callback(error, req.body.uploadedFileName);
    }
})

// Set the storage we declared above.
// .single means we only upload one file at a time.
const upload = multer({storage: uploadsStorage}).single('file');

// State that the uploads dir is the destination for the static files we want to send the client.
// Neccessary for the getFile rout.
app.use(express.static(uploadsDir));

// Create a whiteist for addresses that can reach the server.
var whitelist = ['http://localhost:3000']

// Configure cors library options to allow the whitelist sites to access the server.
// Needs to be done BEFORE the routes decleration.
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors(corsOptions));

// Declare the routes. 
app.get('/', function (req, res) {
  res.send();
});

app.post('/Upload', (req, res) => {
    upload(req, res, function(error) {
        // Check if the user entered a file.
        // I compare it to a string because it sends the request and it's attributes as a json
        // object. I did not find a way to avoid it, please change my code if you do. 
        if(req.body.file === "null"){
            res.statusMessage = "You did not choose a file. Upload rejected. Please try again.";
            res.sendStatus(400);
        }
        // Check if the user entered a name to the file.
        else if((req.body.uploadedFileName === "") ||(req.body.uploadedFileName === null)){
            res.statusMessage = "You did not enter a name. Upload rejected. Please try again."; 
            res.sendStatus(400);
        }
        // You can catch the multer error, in case nothing was wrng with your code and multer 
        // fucked up for some reason.
        else if(error instanceof multer.MulterError){
            res.sendStatus(500);
            res.statusMessage = "Something went wrong with multer during the upload: " + error.message;   
        }
        else if(error){
            // This is where you fucked up. The statusMessage attribute is there to set the 'statusText'
            // field in the respose the client gets.
            res.statusMessage = error.message;
            res.sendStatus(500);
        }
        else{
            console.log("upload happened");
            res.status(200);
            // Sending the fileto the client for further use. 
            res.send(req.body.file);
        }

    });
})

// Serves a static file from the uploads dir.
// Sends the file as is. A txt file will return it's text as plain text, and a video will send it's
// bytes as they are.
// Need to consider some type of encryption.
app.get('/GetFile/:username/:filename', function(req, res){
    console.log("GetFile happened");
    if((!req.params.filename) || (req.params.filename === "")){
        console.log(req.params.filename);
        res.statusMessage = "You did not enter a file name. Enter a file name and try again.";
        res.sendStatus(400);
    }
    else{
        var filePath = path.resolve(uploadsDir, req.params.username, req.params.filename);
        
        if(!fs.existsSync(filePath)){
            res.statusMessage = "The file does not exist. Try a different file name.";
            res.sendStatus(404);
        }
        else{
            res.statusCode = 200;
            res.sendFile(filePath);
        }
    }
})

// Just your regular listener. Out there.. listening..
app.listen(4000, function () {
  console.log("You will never catch me alive.");
});