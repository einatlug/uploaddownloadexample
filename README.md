Hello there!

This is a POC for uploading and downloading files using node js server and React js client.
For the upoad part, I used Multer middleware, it gives me the capability to choose where to save my files, and their names contaraints.
Notice that when you save a file, it will be saved under the name you provided in the "File name" field, and not in it's original name.

This is multer's documentation: https://www.npmjs.com/package/multer

Every file is saved under the directory of your username. It is determined by the username property of the App.js component state (In the client), 
feel free to change it and "log in" as a different user.

The download part simply serves a static file that matches the name you entered in the "File name" field.
It gets the file from the directory of your username (again, you can change that from the client component state).
I added comments everywhere (I really mean everywhere) so you understand exactly what I did.

Happy Coding! :)

To activate the project:

Client:
1. Open a new terminal.
2. go to 'client' directory, execute `npm install`.
3. in this directory, execute `npm start`
4. The client runs on port 3000.

Server: (open a different terminal)
1. Go to 'server' directory, execute `npm install`.
2. To run with nodemon: execute `npm run dev`. This will restart the server every time you save the code.
3. To run with node (when not in development mode): execute `npm start`.
4. The server runs on port 4000.

Note that for every route there is a compatible log. 
The server logs will appear in the terminal of the server project, and the client logs will appear in the chrome developer tool.
The response from the server is logged to the console for every request, and so is the form data of the upload request. 