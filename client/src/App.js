import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
  constructor(){
    super();
    this.state = {
      username: "User1",
      uploadedFile: null,
      uploadedFileName: "",
      requestedFileName: "",
      uploadError: ""
    }

    this.handleChange = this.handleChange.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
    this.getFileByUser = this.getFileByUser.bind(this);
  }

  getFileByUser(event){
    event.preventDefault();
    if((this.state.username === "") || (this.state.requestedFileName === "")){
      console.error("Does not have username or file name. check the data! GetFile filed.");
    }
    else{
      axios.get("http://localhost:4000/GetFile/" + this.state.username + "/" + this.state.requestedFileName)
        .then((response) => {
          console.log(response);
        }).catch((error) =>{
          console.log(error);
        })
    }
  }

  handleChange({ target }) {
    if(target.name == 'uploadedFile'){
      this.setState({uploadedFile: target.files[0]})
    }
    else{
      this.setState({
        [target.name]: target.value
      });
    }
  }

  uploadFile = (event) => {
    event.preventDefault();
    console.log(this.state.uploadedFile);

    // The file must be appended LAST!
    // The order is importand because if the file will bw sent first,
    // the multer nodule will not be able to read the other params when setting the destination
    // for example.
    const formData = new FormData();
    formData.append('uploadedFileName', this.state.uploadedFileName);
    formData.append('username', this.state.username);
    formData.append('file', this.state.uploadedFile);
    console.log(formData);

    let requestInstance = axios.create({
      headers:{
        'content-type': 'multipart/form-data'
      }
    });

    requestInstance.post("http://localhost:4000/Upload", formData).then((response) => {
        this.setState({uploadError: ""});
    }).catch((error) => {
        let errorMsg = error.response.statusText;
        this.setState({uploadError: errorMsg});
    })
  }

  render(){
    return (
      <div className="App">
        <div>
          <h2>Upload File</h2>
          <form onSubmit={this.uploadFile}>
            <label for="uploadedFileName">File Name:</label><br/>
            <input type="text" id="uploadedFileName" name="uploadedFileName" onChange={this.handleChange}/><br/>
            <label for="uploadedFile">File Path:</label><br/>
            <input type="file" id="uploadedFile" name="uploadedFile" onChange={this.handleChange}/>
            <input type="submit" value="Upload"/>
          </form>
        </div>
        <p>{this.uploadError}</p>
        <div>
          <h2>Get File</h2>
          <form onSubmit={this.getFileByUser}>
            <label for="requestedFileName">File Name:</label><br/>
            <input id="requestedFileName" name="requestedFileName" onChange={this.handleChange}/>
            <input type="submit" value="Get File"/>
          </form>
        </div>
      </div>
    );
  }
}

export default App;
